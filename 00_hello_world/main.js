// example 1 encender y apagar el led
const { Board, Led } = require('johnny-five');

const OPTIONS = Object.freeze({
    arduino_uno: {
        PORT: 13,
        PORT_SO: '/dev/ttyACM0' // especificamos el puerto donde se conecta el dispositivo
    }
});

const board = new Board({ 
    port: OPTIONS.arduino_uno.PORT_SO   
});

board.on('ready', function() {
    // console.log('dispositivo listo');

    const led = new Led( OPTIONS.PORT );
    led.blink( 500 );  

    // this hace referencia a la variable board instanciada
    this.repl.inject({ led_13: led  });
});

// captura los errores y los muestra en consola
board.on('error', function( error ) {
    console.log( error );
});

