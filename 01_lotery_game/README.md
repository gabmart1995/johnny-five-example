## 01_lotery_game

Game of lotery, developed with johnny-five and arduino uno. To win just press the button if it matches the number you win the game

- It consists of a push button and a resistor.
- The digital port used is number 2
- This example is for running events from a physical device. 

### references
["johnny-five button"](http://johnny-five.io/examples/button/)


### plans
[![plans-lotery](http://johnny-five.io/img/breadboard/button.png)](http://johnny-five.io/img/breadboard/button.png)


