const { Board, Button } = require('johnny-five');

/**
 * Genera un numero aleatorio.
 * @param {number} min valor minimo del rango
 * @param {number} max valor maximo del rango
 * @returns {number}
 */
function generateNumber( min = 1, max = 100 ) {

    let value = Math.random() * ( max - min ) + min;
    value = Math.ceil( value );

    return value;
}

/**
 * Evento que inicia el juego   
 * @returns {void}
 */
function initGame() {

    let numberSelected = generateNumber( 1, 10 ); 
        
    if ( winner === numberSelected ) {

        console.log({ winner, numberSelected });
        console.log('Felicidades ganaste el juego, resultado en el intento %d', intents );
        
        intents = 1; // reset counter
        winner = generateNumber( 1, 10 ); // new random value

        return;
    } 

    console.log('Lo siento, vuelve a intentar nuevamente');
    
    // add 1 to intents
    intents++;
}

/**
 * Funcion que prepara la tarjeta
 * @returns {void}
 */
function readyBoard() {

    // button en el puerto digital 2 
    const button = new Button({ pin: OPTIONS.arduino_uno.DIGITAL_PORT });

    // verificamos si el repl esta activo (debug)
    if ( this.repl ) {
        this.repl.inject({ button });
    }

    // events button
    button.on('down', initGame );

    console.log('Bienvendo a la loteria, pulsa el boton para ser el ganador');
    console.log('Control + c o Control + d, para salir');
}

// main.js 

const OPTIONS = Object.freeze({
    arduino_uno: {
        DIGITAL_PORT: 2,
        PORT_SO: '/dev/ttyACM0' // especificamos el puerto donde se conecta el dispositivo
    }
});

let winner = generateNumber( 1, 10 );
let intents = 1;  // intentos

const board = new Board({
    port: OPTIONS.arduino_uno.PORT_SO,
    repl: true
});

board.on('ready', readyBoard );